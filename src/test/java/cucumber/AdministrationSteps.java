package cucumber;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.Random;

import clientsimulator.client.DTUPay.CustomerAPIClient;
import clientsimulator.client.DTUPay.MerchantAPIClient;
import clientsimulator.client.DTUPay.REST.requests.CreateCustomerDTO;
import clientsimulator.client.DTUPay.REST.requests.CreateMerchantDTO;

import clientsimulator.client.DTUPay.responses.Customer;
import clientsimulator.client.DTUPay.responses.EmptyResponse;
import clientsimulator.client.DTUPay.responses.Merchant;
import clientsimulator.client.DTUPay.responses.Response;
import clientsimulator.client.bank.responses.DTUPayEmptyResponse;
import clientsimulator.client.bank.responses.DTUPayResponse;
import dtu.ws.fastmoney.Account;
import dtu.ws.fastmoney.ObjectFactory;
import dtu.ws.fastmoney.User;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

/**
 * @author Roberts, Nick
 */
public class AdministrationSteps {
    private TestContext testContext;

    public AdministrationSteps(TestContext testContext) {
    	this.testContext = testContext;
	}

	@Before
	public void setup() {

	}

	@After
	public void cleanup() {
		for (String accountId : testContext.getBankAccountsForCleanup()) {
			DTUPayEmptyResponse response = testContext.getBankService().retireAccount(accountId);
			if (!response.success()) {
				System.out.println("Bank account " + accountId + " was not cleaned up. " +
						"Reason: " + response.getErrorMessage().getMessage());
			}
		}
		for (Customer customer : testContext.getCustomersForCleanup()) {
			// TODO: Do customerClient.deleteCustomer(customer) here when implemented
		}
		for (Merchant merchant : testContext.getMerchantsForCleanup()) {
			// TODO Do merchantClient.deleteMerchant(merchant) here when implemented
		}
	}

	public void createBankAccountForCleanup(User user, BigDecimal balance) {
		// Check if account already exists, and if so, delete it
		DTUPayResponse<Account> existingAccount = testContext.getBankService().getAccountByCprNumber(user.getCprNumber());
		if (existingAccount.success() && existingAccount.getResponse() != null) {
			DTUPayEmptyResponse response = testContext.getBankService().retireAccount(existingAccount.getResponse().getId());
			assertTrue("Existing bank account could not be deleted", response.success());
		}
		System.out.println("Creating bank account " + user.getFirstName() + " " + user.getLastName() + " " + user.getCprNumber());
		// Create new account
		DTUPayResponse<String> response = testContext.getBankService().createAccountWithBalance(user, balance);
		assertTrue("Bank account could not be created", response.success());
		testContext.getBankAccountsForCleanup().add(response.getResponse());
	}

	public void createCustomerAndBankAccountForCleanup(BigDecimal balance) {
		Customer customer = new Customer();
		customer.cpr = String.valueOf(new Random().nextInt(99999999));
		customer.firstName = "Customer_FirstName";
		customer.lastName = "Customer_LastName";
		EmptyResponse response = testContext.getCustomerClient().createCustomer(customer.firstName,
				customer.lastName, customer.cpr);
		assertTrue(response.toString(), response.success());

		testContext.getCustomersForCleanup().add(customer);
		testContext.setCurrentCustomer(customer);

		// Create bank account
		createUser(customer.firstName, customer.lastName, customer.cpr);
		createBankAccountForCleanup(testContext.getCurrentUser(), balance);

		testContext.setOriginalCustomerBalance(balance);
		testContext.setCurrentCustomerBalance(balance);
	}

	public void createMerchantAndBankAccountForCleanup(BigDecimal balance) {
		Merchant merchant = new Merchant();
		merchant.cpr = String.valueOf(new Random().nextInt(99999999));
		merchant.firstName = "Merchant_FirstName";
		merchant.lastName = "Merchant_LastName";
		EmptyResponse response = testContext.getMerchantClient().createMerchant(merchant.firstName,
				merchant.lastName, merchant.cpr);
		assertTrue(response.toString(), response.success());

		testContext.getMerchantsForCleanup().add(merchant);
		testContext.setCurrentMerchant(merchant);

		// Create bank account
		createUser(merchant.firstName, merchant.lastName, merchant.cpr);
		createBankAccountForCleanup(testContext.getCurrentUser(), balance);

		testContext.setOriginalMerchantBalance(balance);
		testContext.setCurrentMerchantBalance(balance);
	}

	private void createUser(String firstName, String lastName, String cpr) {
        User user = new ObjectFactory().createUser();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setCprNumber(cpr);
        testContext.getFirstNameUsers().put(firstName, user);
        testContext.setCurrentUser(user);
    }
	@Given("a user")
	public void givenUser() {
		String randomCpr= String.valueOf(new Random().nextInt(999999));
		createUser("First", "User", randomCpr);
	}
	@Given("a first name")
	public void givenFirstName() {
	testContext.setCurrentFirstName("Test");
	}
	@Given("a last name")
	public void givenLastName() {
	testContext.setCurrentLastName("User");
	}
	@Given("a valid cpr")
	public void givenValidCpr() {
	String randomCpr= String.valueOf(new Random().nextInt(999999999));
	testContext.setCurrentCpr(randomCpr);
	}

	@Given("a duplicate cpr for merchant")
	public void givenDuplicateCprMerchant() {
		String randomCpr= String.valueOf(new Random().nextInt(999999999));
		
		Merchant merchant = new Merchant();
		merchant.cpr = randomCpr;
		merchant.firstName = testContext.currentFirstName;
		merchant.lastName = testContext.currentLastName;
		EmptyResponse response = testContext.getMerchantClient().createMerchant(merchant.firstName,
				merchant.lastName, merchant.cpr);
		assertTrue(response.toString(), response.success());
		//testContext.setCurrentMerchantResponse(response);
		testContext.getMerchantsForCleanup().add(merchant);
		testContext.setCurrentMerchant(merchant);

		// Create bank account
		BigDecimal balance=new BigDecimal(10);
		createUser(merchant.firstName, merchant.lastName, merchant.cpr);
		createBankAccountForCleanup(testContext.getCurrentUser(), balance);

		testContext.setOriginalMerchantBalance(balance);
		testContext.setCurrentMerchantBalance(balance);
		
		testContext.setCurrentCpr(randomCpr);
		testContext.setCurrentLastName("Test");
		testContext.setCurrentLastName("User");
	}

	@Given("a duplicate cpr for customer")
	public void givenDuplicateCprCustomer() {
		String randomCpr = String.valueOf(new Random().nextInt(999999999));
		
		
		Customer customer = new Customer();
		customer.cpr = randomCpr;
		customer.firstName = testContext.currentFirstName;
		customer.lastName = testContext.currentLastName;
		EmptyResponse response = testContext.getCustomerClient().createCustomer(customer.firstName,
				customer.lastName, customer.cpr);
		assertTrue(response.toString(), response.success());
		
		testContext.setCurrentCustomerResponse(response);
		testContext.getCustomersForCleanup().add(customer);
		testContext.setCurrentCustomer(customer);

		BigDecimal balance = new BigDecimal(10);
		// Create bank account
		createUser(customer.firstName, customer.lastName, customer.cpr);
		createBankAccountForCleanup(testContext.getCurrentUser(), balance);

		testContext.setOriginalCustomerBalance(balance);
		testContext.setCurrentCustomerBalance(balance);

		testContext.setCurrentCpr(randomCpr);
		testContext.setCurrentLastName("Test");
		testContext.setCurrentLastName("User");
	}

	@Given("a customer")
	public void givenACustomer() {
		givenACustomerWith0Balance();
	}

	@Given("a merchant")
	public void givenAMerchant() {
		givenAMerchantWith0Balance();
	}

	@Given("a customer with {int} token(s)")
	public void givenACustomerWithNTokens(int amount) {
		givenACustomer();
    	if (amount > 5) {
			new TokenSteps(testContext).givenNTokens(1);
			if (testContext.getLastTokenResponse() != null) {
				assertTrue("Unable to create tokens: " + testContext.getLastTokenResponse().toString(),
						testContext.getLastTokenResponse().success());
			}
			new TokenSteps(testContext).givenNTokens(amount - 1);
		} else {
    		new TokenSteps(testContext).givenNTokens(amount);
		}

		if (testContext.getLastTokenResponse() != null) {
			assertTrue("Unable to create tokens: " + testContext.getLastTokenResponse().toString(),
					testContext.getLastTokenResponse().success());
		}
	}

	@Given("a customer with a positive balance")
	public void givenACustomerWithAPositiveBalance() {
		BigDecimal balance = new BigDecimal(new Random().nextInt(Integer.MAX_VALUE - 1) + 1);
		createCustomerAndBankAccountForCleanup(balance);
	}

	@Given("a merchant with a positive balance")
	public void givenAMerchantWithAPositiveBalance() {
		BigDecimal balance = new BigDecimal(new Random().nextInt(Integer.MAX_VALUE - 1) + 1);
		createMerchantAndBankAccountForCleanup(balance);
	}

	@Given("a customer with 0 balance")
	public void givenACustomerWith0Balance() {
    	BigDecimal balance = new BigDecimal(0);
    	createCustomerAndBankAccountForCleanup(balance);
	}

	@Given("a merchant with 0 balance")
	public void givenAMerchantWith0Balance() {
    	BigDecimal balance = new BigDecimal(0);
    	createMerchantAndBankAccountForCleanup(balance);
	}


	@When("creating a merchant")
	public void creatingMerchant() {
		//Merchant merchant = testContext.getCurrentMerchantRequest();
//
	//	testContext.setCurrentMerchantResponse(testContext.getMerchantClient().createMerchant(merchant.firstName, merchant.lastName,merchant.cpr));
		Merchant merchant = new Merchant();
		merchant.cpr = testContext.currentCpr;
		merchant.firstName = testContext.currentFirstName;
		merchant.lastName = testContext.currentLastName;
		EmptyResponse response = testContext.getMerchantClient().createMerchant(merchant.firstName,
				merchant.lastName, merchant.cpr);
		//assertTrue(response.toString(), response.success());
		testContext.setCurrentMerchantResponse(response);
		testContext.getMerchantsForCleanup().add(merchant);
		testContext.setCurrentMerchant(merchant);

		// Create bank account
		BigDecimal balance=new BigDecimal(10);
		createUser(merchant.firstName, merchant.lastName, merchant.cpr);
		createBankAccountForCleanup(testContext.getCurrentUser(), balance);

		testContext.setOriginalMerchantBalance(balance);
		testContext.setCurrentMerchantBalance(balance);
	}


	@When("creating a customer")
	public void creatingCustomer() {
		//Customer customer = testContext.getCurrentCustomerRequest();
		//testContext.setCurrentCustomerResponse(testContext.getCustomerClient().createCustomer(customer.firstName, customer.lastName,customer.cpr));
		Customer customer = new Customer();
		customer.cpr = testContext.currentCpr;
		customer.firstName = testContext.currentFirstName;
		customer.lastName = testContext.currentLastName;
		EmptyResponse response = testContext.getCustomerClient().createCustomer(customer.firstName,
				customer.lastName, customer.cpr);
		//assertTrue(response.toString(), response.success());
		
		testContext.setCurrentCustomerResponse(response);
		testContext.getCustomersForCleanup().add(customer);
		testContext.setCurrentCustomer(customer);

		BigDecimal balance = new BigDecimal(10);
		// Create bank account
		createUser(customer.firstName, customer.lastName, customer.cpr);
		createBankAccountForCleanup(testContext.getCurrentUser(), balance);

		testContext.setOriginalCustomerBalance(balance);
		testContext.setCurrentCustomerBalance(balance);
	}

	@Then("the merchant is created")
	public void merchantHasBeenCreated() {
		assertTrue(testContext.getCurrentMerchantResponse().success());
	}

	@Then("the customer is created")
	public void customerHasBeenCreated() {
		assertTrue(testContext.getCurrentCustomerResponse().success());
	}

	@Then("the merchant is not created")
	public void merchantHasNotBeenCreated() {
	assertFalse(testContext.getCurrentMerchantResponse().success());
	}

	@Then("the customer is not created")
	public void customerHasNotBeenCreated() {
	assertFalse(testContext.getCurrentCustomerResponse().success());
	}
}