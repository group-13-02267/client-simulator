package cucumber;

import clientsimulator.client.DTUPay.REST.requests.RequestTokensDTO;
import clientsimulator.client.DTUPay.REST.responses.DTUResponse;
import clientsimulator.client.DTUPay.responses.Merchant;
import clientsimulator.client.DTUPay.responses.Response;
import clientsimulator.client.DTUPay.responses.Token;
import clientsimulator.client.DTUPay.responses.TokenResponse;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.*;


/**
 * @author Nick
 */
public class TokenSteps {

    private TestContext testContext;

    public TokenSteps(TestContext testContext) {
        this.testContext = testContext;
    }

    private void requestTokens(String customerId, int amount) {
        /*RequestTokensDTO requestTokensDTO = new RequestTokensDTO();
        requestTokensDTO.amount = amount;
        Object obj=testContext.getCustomerClient().requestTokens(customerId, requestTokensDTO.amount);
        LinkedTreeMap<Object,Object> t = (LinkedTreeMap<Object, Object>) obj;
        String id=t.get("id").toString();
        boolean status=(boolean)t.get("status");

        List<LinkedTreeMap<String, String>> tokenMaps=(List<LinkedTreeMap<String, String>>)t.get("tokens");
        List<Token> tokens = new ArrayList<>();

        for (LinkedTreeMap<String, String> tokenMap : (List<LinkedTreeMap>) response.getValue().tokens) {
            Token token = new Token(tokenMap.get("value"));
            tokens.add(token);
        }

        TokenResponse tokenResponse=new TokenResponse(id, tokens, status);
        Response<TokenResponse> response = new DTUResponse<TokenResponse>(tokenResponse);
        */

        Response<TokenResponse> response = testContext.getCustomerClient().requestTokens(customerId, amount);
        //Response<TokenResponse> response = testContext.getCustomerClient().requestTokens(customerId, amount);
        testContext.setLastTokenResponse(response);

        if (response.success()) {
            testContext.getValidTokens().addAll(response.getValue().tokens);
        }
    }

    @Given("{int} token(s)")
    public void givenNTokens(int amount) {
        if (amount > 0) {
            requestTokens(testContext.getCurrentCustomer().cpr, amount);
        }
    }

    @Given("a valid token")
    public void givenAValidToken() {
        requestTokens(testContext.getCurrentCustomer().cpr, 1);
    }

    @Given("an invalid token")
    public void givenAnInvalidToken() {
        // The token is invalid only because it has not been created by DTUPay
        Token token = new Token();
        token.value = "invalid_token";
        testContext.getValidTokens().add(token);
    }

    @When("the customer requests {int} token(s)")
    public void whenTheCustomerRequestsNTokens(int amount) {
        requestTokens(testContext.getCurrentCustomer().cpr, amount);
    }

    @Then("the customer has {int} token(s)")
    public void thenTheCustomerHasNTokens(int amount) {
        assertEquals(testContext.getLastTokenResponse().toString(), amount, testContext.getValidTokens().size());
    }

    @Then("the token request is denied")
    public void thenTheRequestWasDenied() {
        assertFalse(testContext.getLastTokenResponse().toString(), testContext.getLastTokenResponse().success());
    }

    @Then("the token becomes invalid")
    public void thenTheTokenBecomesInvalid() {
      Merchant merchant = testContext.getCurrentMerchant();
      Token token = testContext.getUsedTokens().get(testContext.getUsedTokens().size() - 1);
      //   Check token validity by trying to do a payment (very hacky, but there's no other way)
      new PaymentSteps(testContext).doPayment(merchant.cpr, token, 1f);
      assertFalse("Token was valid, expected to be invalid", testContext.getLastPaymentResponse().success());
    }

}
