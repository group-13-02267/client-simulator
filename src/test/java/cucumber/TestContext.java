package cucumber;

import clientsimulator.client.DTUPay.CustomerAPIClient;
import clientsimulator.client.DTUPay.REST.DTUCustomerRestClient;
import clientsimulator.client.DTUPay.REST.DTUMerchantRestClient;
import clientsimulator.client.DTUPay.MerchantAPIClient;
import clientsimulator.client.DTUPay.REST.requests.CreateCustomerDTO;
import clientsimulator.client.DTUPay.REST.requests.CreateMerchantDTO;
import clientsimulator.client.DTUPay.responses.*;
import clientsimulator.client.bank.BankService;
import clientsimulator.client.bank.DTUSoapBankService;
import dtu.ws.fastmoney.User;

import java.io.Console;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Holder class for shared cucumber test steps.
 */
public class TestContext {

    public List<String> bankAccountsForCleanup = new ArrayList<>();
    public List<Customer> customersForCleanup = new ArrayList<>();
    public List<Merchant> merchantsForCleanup = new ArrayList<>();

    public CustomerAPIClient customerClient = new DTUCustomerRestClient();
    public MerchantAPIClient merchantClient = new DTUMerchantRestClient();
    public BankService bankService = new DTUSoapBankService();

    public Customer currentCustomer;
    public Merchant currentMerchant;
    public BigDecimal originalCustomerBalance;
    public BigDecimal originalMerchantBalance;
    public BigDecimal currentCustomerBalance;
    public BigDecimal currentMerchantBalance;
    public EmptyResponse lastPaymentResponse;
    public EmptyResponse lastRefundResponse;

    public List<Token> validTokens = new ArrayList<>();
    public List<Token> usedTokens = new ArrayList<>();
    public Response<TokenResponse> lastTokenResponse;

    public String currentFirstName;
    public String currentLastName;
    public String currentCpr;
    public EmptyResponse currentMerchantResponse;
    public EmptyResponse currentCustomerResponse;
    public User currentUser;

    public Map<String, User> firstNameUsers = new HashMap<>();

    CustomerReport currentCustomerReport;
    Response<CustomerReport> currentCustomerReportResponse;
    MerchantReport currentMerchantReport;
    Response<MerchantReport> currentMerchantReportResponse;

    public TestContext() {

    }

    public List<String> getBankAccountsForCleanup() {
        return bankAccountsForCleanup;
    }

    public void setBankAccountsForCleanup(List<String> bankAccountsForCleanup) {
        this.bankAccountsForCleanup = bankAccountsForCleanup;
    }

    public List<Customer> getCustomersForCleanup() {
        return customersForCleanup;
    }

    public void setCustomersForCleanup(List<Customer> customersForCleanup) {
        this.customersForCleanup = customersForCleanup;
    }

    public List<Merchant> getMerchantsForCleanup() {
        return merchantsForCleanup;
    }

    public void setMerchantsForCleanup(List<Merchant> merchantsForCleanup) {
        this.merchantsForCleanup = merchantsForCleanup;
    }

    public CustomerAPIClient getCustomerClient() {
        return customerClient;
    }

    public void setCustomerClient(CustomerAPIClient customerClient) {
        this.customerClient = customerClient;
    }

    public MerchantAPIClient getMerchantClient() {
        return merchantClient;
    }

    public void setMerchantClient(MerchantAPIClient merchantClient) {
        this.merchantClient = merchantClient;
    }

    public BankService getBankService() {
        return bankService;
    }

    public void setBankService(BankService bankService) {
        this.bankService = bankService;
    }

    public Customer getCurrentCustomer() {
        return currentCustomer;
    }

    public void setCurrentCustomer(Customer currentCustomer) {
        this.currentCustomer = currentCustomer;
        this.currentFirstName = currentCustomer.firstName;

    }

    public Merchant getCurrentMerchant() {
        return currentMerchant;
    }

    public void setCurrentMerchant(Merchant currentMerchant) {
        this.currentMerchant = currentMerchant;
    }

    public BigDecimal getCurrentCustomerBalance() {
        return currentCustomerBalance;
    }

    public void setCurrentCustomerBalance(BigDecimal currentCustomerBalance) {
        this.currentCustomerBalance = currentCustomerBalance;
    }

    public BigDecimal getCurrentMerchantBalance() {
        return currentMerchantBalance;
    }

    public void setCurrentMerchantBalance(BigDecimal currentMerchantBalance) {
        this.currentMerchantBalance = currentMerchantBalance;
    }

    public List<Token> getValidTokens() {
        return validTokens;
    }

    public void setValidTokens(List<Token> validTokens) {
        this.validTokens = validTokens;
    }

    public Response<TokenResponse> getLastTokenResponse() {
        return lastTokenResponse;
    }

    public void setLastTokenResponse(Response<TokenResponse> response) {
        this.lastTokenResponse = response;
    }

    public String getCurrentFirstName() {
    	return currentFirstName;
    }

    public void setCurrentFirstName(String name) {
    	this.currentFirstName=name;
    }

    public String getCurrentLastName() {
    	return currentLastName;
    }

    public void setCurrentLastName(String name) {
    	this.currentLastName=name;
    }

    public String getCurrentCpr() {
    	return currentCpr;
    }

    public void setCurrentCpr(String cpr) {
    	this.currentCpr=cpr;
    }

    public EmptyResponse getCurrentMerchantResponse(){
    	return currentMerchantResponse;
    }

    public void setCurrentMerchantResponse(EmptyResponse resp) {
    	this.currentMerchantResponse=resp;
    }

    public EmptyResponse getCurrentCustomerResponse(){
    	return currentCustomerResponse;
    }

    public void setCurrentCustomerResponse(EmptyResponse resp) {
    	this.currentCustomerResponse=resp;
    }

    public User getCurrentUser() {
    	return currentUser;
    }

    public void setCurrentUser(User user) {
    	this.currentUser=user;
    }

    public Merchant getCurrentMerchantRequest() {
    	return new Merchant(currentFirstName,currentLastName,currentCpr);
    }

    public Customer getCurrentCustomerRequest() {
    	return new Customer(currentFirstName,currentLastName,currentCpr);
    }

    public Response<CustomerReport> getCurrentCustomerReportResponse(){
    	return currentCustomerReportResponse;
    }

    public void setCurrentCustomerReportResponse(Response<CustomerReport> resp) {
    	currentCustomerReportResponse=resp;
    }

    public Response<MerchantReport> getCurrentMerchantReportResponse(){
    	return currentMerchantReportResponse;
    }

    public void setCurrentMerchantReportResponse(Response<MerchantReport> resp) {
    	currentMerchantReportResponse=resp;
    }

    public CustomerReport getCurrentCustomerReport(){
    	return currentCustomerReport;
    }

    public void setCurrentCustomerReport(CustomerReport report) {
    	currentCustomerReport=report;
    }

    public MerchantReport getCurrentMerchantReport(){
    	return currentMerchantReport;
    }

    public void setCurrentMerchantReport(MerchantReport report) {
    	currentMerchantReport=report;
    }

    public Map<String, User> getFirstNameUsers() {
        return firstNameUsers;
    }

    public void setFirstNameUsers(Map<String, User> firstNameUsers) {
        this.firstNameUsers = firstNameUsers;
    }

    public EmptyResponse getLastPaymentResponse() {
        return lastPaymentResponse;
    }

    public void setLastPaymentResponse(EmptyResponse response) {
        this.lastPaymentResponse = response;
    }

    public BigDecimal getOriginalCustomerBalance() {
        return originalCustomerBalance;
    }

    public void setOriginalCustomerBalance(BigDecimal originalCustomerBalance) {
        this.originalCustomerBalance = originalCustomerBalance;
    }

    public BigDecimal getOriginalMerchantBalance() {
        return originalMerchantBalance;
    }

    public void setOriginalMerchantBalance(BigDecimal originalMerchantBalance) {
        this.originalMerchantBalance = originalMerchantBalance;
    }

    public EmptyResponse getLastRefundResponse() {
        return lastRefundResponse;
    }

    public void setLastRefundResponse(EmptyResponse lastRefundResponse) {
        this.lastRefundResponse = lastRefundResponse;
    }

    public List<Token> getUsedTokens() {
        return usedTokens;
    }

    public void setUsedTokens(List<Token> usedTokens) {
        this.usedTokens = usedTokens;
    }

    public Token getNextToken() {
        return validTokens.get(validTokens.size() - 1);
    }
}
