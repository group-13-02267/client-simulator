package cucumber;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import clientsimulator.client.DTUPay.CustomerAPIClient;
import clientsimulator.client.DTUPay.MerchantAPIClient;
import clientsimulator.client.DTUPay.REST.requests.CreatePaymentDTO;
import clientsimulator.client.DTUPay.responses.*;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import static org.junit.Assert.*;

public class ReportingSteps{
	String startDate;
	String endDate;
	private TestContext testContext;
	private MerchantAPIClient merchantAPI;
	private CustomerAPIClient customerAPI;

	public ReportingSteps(TestContext testContext) {
        this.testContext = testContext;
    }

    public EmptyResponse doPayment(String merchantId, Token token, float amount) {
    	
        return testContext.merchantClient.createPayment(merchantId, token, amount);
    }
	
	@Given("a valid start date")
	public void givenStartDate() {
		startDate="01-01-2020";
	}
	@Given("an invalid start date")
	public void givenInvalidStartDate() {
		startDate="01-01-2000";
	}
	@Given("a valid end date")
	public void givenEndDate() {
		endDate="31-01-2020";
	}
	@Given("an invalid end date")
	public void givenInvalidEndDate() {
		endDate="31-01-2000";
	}

	@When("the customer generates a report")
	public void the_customer_generates_a_report() {
		if(endDate==null) {
			Customer customer = testContext.getCurrentCustomer();
			String cpr = customer.cpr;
			CustomerAPIClient client = testContext.getCustomerClient();
			Response<CustomerReport> report = client.getReport(cpr, startDate);
			testContext.setCurrentCustomerReportResponse(report);
		} else {
			Customer customer = testContext.getCurrentCustomer();
			String cpr = customer.cpr;
			CustomerAPIClient client = testContext.getCustomerClient();
			Response<CustomerReport> report = client.getReport(cpr, startDate, endDate);
			testContext.setCurrentCustomerReportResponse(report);
		}
	}

	@When("the merchant generates a report")
	public void merchantGeneratesReport() {
		if(endDate==null) {
			Merchant merchant = testContext.getCurrentMerchant();
			String cpr = merchant.cpr;
			MerchantAPIClient client = testContext.getMerchantClient();
			Response<MerchantReport> report = client.getReport(cpr, startDate);
			testContext.setCurrentMerchantReportResponse(report);
		} else {
			Merchant merchant = testContext.getCurrentMerchant();
			String cpr = merchant.cpr;
			MerchantAPIClient client = testContext.getMerchantClient();
			Response<MerchantReport> report = client.getReport(cpr, startDate, endDate);
			testContext.setCurrentMerchantReportResponse(report);;
		}
	}

	@Then("the customer report is retrieved")
	public void customerReportRetrieved() {
		assertTrue(testContext.getCurrentCustomerReportResponse().success());
		testContext.setCurrentCustomerReport(testContext.getCurrentCustomerReportResponse().getValue());
	}
	@Then("the merchant report is retrieved")
	public void merchantReportRetrieved() {
		assertTrue(testContext.getCurrentMerchantReportResponse().success());
		testContext.setCurrentMerchantReport(testContext.getCurrentMerchantReportResponse().getValue());
	}
	@Then("the customer report contains a transaction")
	public void customerReportContainsTransaction() {
		CustomerReport report=testContext.getCurrentCustomerReport();
		Type listType = new TypeToken<List<CustomerTransaction>>(){}.getType();
		System.out.println("Report transactions: " + report.report);
		List<CustomerTransaction> transactionDTOS = new Gson().fromJson(report.report, listType);
		assertFalse(transactionDTOS.isEmpty());
		assertEquals(1, transactionDTOS.size());
		assertFalse(report.report.isEmpty());
	}
	@Then("the merchant report contains a transaction")
	public void merchantReportContainsTransaction() {
		MerchantReport report=testContext.getCurrentMerchantReport();
		assertFalse(report.report.isEmpty());
	}
	@Then("the customer transactions contain information about token and amount")
	public void transactionsContainInformationForCustomer() {
		Type listType = new TypeToken<List<CustomerTransaction>>(){}.getType();
		List<CustomerTransaction> transactionDTOS = new Gson().fromJson(testContext.getCurrentCustomerReport().report, listType);
		transactionDTOS.forEach((transaction->{
			assertNotNull(transaction.getAmount());
			assertNotNull(transaction.getToken());
		}));
	}

	@Then("the merchant transactions contain information about token and amount")
	public void transactionsContainInformationForMerchant() {
		Type listType = new TypeToken<ArrayList<MerchantTransaction>>(){}.getType();
		ArrayList<MerchantTransaction> transactionDTOS = new Gson().fromJson(testContext.getCurrentMerchantReport().report, listType);
		transactionDTOS.forEach((transaction->{
			assertNotNull(transaction.getAmount());
			assertNotNull(transaction.getToken());
		}));
	}
}