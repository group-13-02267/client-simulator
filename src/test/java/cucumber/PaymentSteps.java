package cucumber;

import clientsimulator.client.DTUPay.responses.EmptyResponse;
import clientsimulator.client.DTUPay.responses.Token;
import clientsimulator.client.bank.responses.DTUPayResponse;
import dtu.ws.fastmoney.Account;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.math.BigDecimal;
import java.util.Random;

import static org.junit.Assert.*;

/**
 * @author Nick
 */
public class PaymentSteps {

    private TestContext testContext;

    public PaymentSteps(TestContext testContext) {
        this.testContext = testContext;
    }

    public void doPayment(String merchantId, Token token, float amount) {
        System.out.println("Payment");
        System.out.println("MerchantID: " + merchantId);
        System.out.println("Token: " + token.value);
        System.out.println("Amount: " + amount);
        EmptyResponse response = testContext.getMerchantClient().createPayment(merchantId, token, amount);
        testContext.setLastPaymentResponse(response);
        testContext.getUsedTokens().add(token);
        testContext.getValidTokens().remove(token);
    }

    public void doRefund(String merchantId, Token token, float amount) {
        System.out.println("Refund");
        System.out.println("MerchantID: " + merchantId);
        System.out.println("Token: " + token.value);
        System.out.println("Amount: " + amount);
        EmptyResponse response = testContext.getMerchantClient().initiateRefund(merchantId, token, amount);
        testContext.setLastRefundResponse(response);
        testContext.getUsedTokens().add(token);
        testContext.getValidTokens().remove(token);

        if (response.success()) {
            BigDecimal refundAmount = new BigDecimal(amount);
            testContext.setCurrentCustomerBalance(testContext.getCurrentCustomerBalance().add(refundAmount));
            testContext.setCurrentMerchantBalance(testContext.getCurrentMerchantBalance().subtract(refundAmount));
        }
    }

    private BigDecimal getBankBalance(String cpr) {
        DTUPayResponse<Account> response = testContext.getBankService().getAccountByCprNumber(cpr);
        assertTrue(response.toString(), response.success());
        return response.getResponse().getBalance();
    }

    private boolean balancesAreEqual(BigDecimal a, BigDecimal b) {
        // Allowed error is equal to 0.001 (necessary due to float-BD conversion)
        BigDecimal allowedError = new BigDecimal(1e-3);
        return a.subtract(b).abs().compareTo(allowedError) < 0;
    }

    @When("the customer makes a purchase from the merchant")
    public void whenTheCustomerMakesAPurchaseFromTheMerchant() {
        BigDecimal currentBalance = testContext.getCurrentCustomerBalance();
        BigDecimal randomPrice;
        if (currentBalance.compareTo(BigDecimal.ZERO) > 0) {
            randomPrice = new BigDecimal(new Random().nextInt(testContext.getCurrentCustomerBalance().intValue()));
        } else {
            randomPrice = BigDecimal.ONE;
        }

        doPayment(testContext.getCurrentMerchant().cpr, testContext.getNextToken(), randomPrice.floatValue());

        if (testContext.getLastPaymentResponse().success()) {
            // Update local balance
           // float amount = testContext.getLastPaymentResponse().getValue().getAmount();
            float amount = randomPrice.floatValue();
        	BigDecimal currentCustomerBalance = testContext.getCurrentCustomerBalance();
            BigDecimal currentMerchantBalance = testContext.getCurrentMerchantBalance();
            testContext.setCurrentCustomerBalance(currentCustomerBalance.subtract(new BigDecimal(amount)));
            testContext.setCurrentMerchantBalance(currentMerchantBalance.add(new BigDecimal(amount)));
        }
    }

    @When("a refund is requested")
    public void whenARefundIsRequested() {
        float amount;
        if (testContext.getCurrentMerchantBalance() != null && testContext.getCurrentMerchantBalance().intValue() > 1) {
            amount = new Random().nextInt(testContext.getCurrentMerchantBalance().intValue() - 1) + 1;
        } else {
            amount = 1f;
        }
        doRefund(testContext.getCurrentMerchant().cpr, testContext.getNextToken(), amount);
    }

    @Then("the transaction fails")
    public void thenTheTransactionFails() {
        assertFalse(testContext.getLastPaymentResponse().success());
    }

    @Then("the refund is successful")
    public void thenTheTransactionIsRefunded() {
        assertNotNull(testContext.getLastRefundResponse());
        assertTrue(testContext.getLastRefundResponse().toString(), testContext.getLastRefundResponse().success());
    }

    @Then("the refund is denied")
    public void thenTheRefundIsDenied() {
        assertNotNull(testContext.getLastRefundResponse());
        assertFalse(testContext.getLastRefundResponse().toString(), testContext.getLastRefundResponse().success());
    }

    @Then("the customer has the correct balance")
    public void thenTheCustomerHasTheCorrectBalance() {
        BigDecimal bankBalance = getBankBalance(testContext.getCurrentCustomer().cpr);
        assertTrue("Customer balance is not consistent with the bank. " +
                        "\nExpected: " + testContext.getCurrentCustomerBalance().toString() +
                        "\nActual: " + bankBalance.toString(),
                balancesAreEqual(testContext.getCurrentCustomerBalance(), bankBalance));
    }

    @Then("the merchant has the correct balance")
    public void thenTheMerchantHasTheCorrectBalance() {
        BigDecimal bankBalance = getBankBalance(testContext.getCurrentMerchant().cpr);
        assertTrue("Merchant balance is not consistent with the bank. " +
                        "\nExpected: " + testContext.getCurrentMerchantBalance().toString() +
                        "\nActual: " + bankBalance.toString(),
                balancesAreEqual(testContext.getCurrentMerchantBalance(), bankBalance));
    }

    @Then("the customer's balance stays the same")
    public void thenTheCustomersBalanceStaysTheSame() {
        BigDecimal bankBalance = getBankBalance(testContext.getCurrentCustomer().cpr);
        assertTrue("Customer bank balance was changed. " +
                        "\nExpected: " + testContext.getOriginalCustomerBalance().toString() +
                        "\nActual: " + bankBalance.toString(),
                balancesAreEqual(testContext.getOriginalCustomerBalance(), bankBalance));
    }

    @Then("the merchant's balance stays the same")
    public void thenTheMerchantsBalanceStaysTheSame() {
        BigDecimal bankBalance = getBankBalance(testContext.getCurrentMerchant().cpr);
        assertTrue("Merchant bank balance was changed. " +
                        "\nExpected: " + testContext.getOriginalMerchantBalance().toString() +
                        "\nActual: " + bankBalance.toString(),
                balancesAreEqual(testContext.getOriginalMerchantBalance(), bankBalance));
    }
}
