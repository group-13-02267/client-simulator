Feature: Refund Feature
  different scenarios using refunds

  Scenario: A valid refund request
    Given a customer with a positive balance
    And 3 tokens
    And a merchant with a positive balance
    When the customer makes a purchase from the merchant
    And a refund is requested
    Then the refund is successful
    And the customer has the correct balance
    And the merchant has the correct balance

  Scenario: A refund request with an invalid token
  	Given a customer
    And an invalid token
    And a merchant with a positive balance
    When a refund is requested
    Then the refund is denied
    And the customer's balance stays the same
    And the merchant's balance stays the same

  Scenario: A merchant does not have enough balance to issue refund
    Given a customer with 1 token
    And a merchant with 0 balance
    When a refund is requested
    Then the refund is denied
    And the customer's balance stays the same
    And the merchant's balance stays the same