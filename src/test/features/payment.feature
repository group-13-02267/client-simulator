Feature: Payment
  Different scenarios including payments

  Scenario: A customer makes a valid purchase from the merchant
  	Given a customer with a positive balance
  	And a valid token
  	And a merchant
  	When the customer makes a purchase from the merchant
  	Then the token becomes invalid
  	And the customer has the correct balance
  	And the merchant has the correct balance

  Scenario: A customer has an invalid token
  	Given a customer with a positive balance
  	And an invalid token
  	And a merchant with a positive balance
  	When the customer makes a purchase from the merchant
  	Then the transaction fails
  	And the customer's balance stays the same
  	And the merchant's balance stays the same

  Scenario: A customer does not have enough balance
  	Given a customer with 0 balance
  	And a valid token
  	And a merchant
  	When the customer makes a purchase from the merchant
  	Then the transaction fails
  	And the customer's balance stays the same
  	And the merchant's balance stays the same

