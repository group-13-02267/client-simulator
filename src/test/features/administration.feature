
Feature: Administration
  different scenarios using administration

  Scenario: User creates a new customer
    Given a user
    And a first name
    And a last name
    And a valid cpr
    When creating a customer
    Then the customer is created

  Scenario: User creates a new customer with a duplicate cpr
    Given a user
    And a first name
    And a last name
    And  a duplicate cpr for customer
    When creating a customer
    Then the customer is not created
      
  #Scenario: User deletes a customer with a valid cpr
    #Given a customer
    #And a valid cpr
    #When deleting a customer
    #Then the customer is deleted  
    #
  #Scenario: User deletes a customer with an invalid cpr
    #Given a user
    #And an invalid cpr
    #When deleting a customer
    #Then no customers are deleted  

    Scenario: A user creates a new merchant
    Given a user
    And a first name
    And a last name
    And a valid cpr
    When creating a merchant
    Then the merchant is created

  Scenario: A user creates a new merchant with a duplicate cpr
    Given a user
    And a first name
    And a last name
    And  a duplicate cpr for merchant
    When creating a merchant
    Then the merchant is not created
      
  #Scenario: A user deletes a merchant with a valid cpr
    #Given a user
    #And a valid cpr
    #When deleting a merchant
    #Then the merchant is deleted  
    #
  #Scenario: A user deletes a merchant with an invalid cpr
    #Given a user
    #And an invalid cpr
    #When deleting a merchant
    #Then no merchants are deleted  
    