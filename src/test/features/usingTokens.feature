Feature: Token Manager
  Different scenarios including tokens

  Scenario: A customer has no tokens, requests 5
    Given a customer with 0 tokens
    When the customer requests 5 tokens
    Then the customer has 5 tokens

  Scenario: A customer has 1 token, requests 5 more
    Given a customer with 1 tokens
    When the customer requests 5 tokens
    Then the customer has 6 tokens

  Scenario: A customer requests more than limit
    Given a customer
    And 0 tokens
    When the customer requests 6 tokens
    Then the token request is denied

  Scenario: A customer has more than 1 token, requests more
    Given a customer with 2 tokens
    When the customer requests 1 token
    Then the token request is denied

  Scenario: A customer has max amount of tokens, requests more
  	Given a customer with 6 tokens
  	When the customer requests 1 token
  	Then the token request is denied
