Feature: Reporting feature
  different scenarios using reporting

  Scenario: A customer requests a report with an end date
    Given a customer with a positive balance
    And a merchant
    And a valid start date
    And a valid end date
    And a valid token
    When the customer makes a purchase from the merchant
    And the customer generates a report
    Then the customer report is retrieved
    And the customer report contains a transaction
    And the customer transactions contain information about token and amount

  Scenario: A customer requests a report without an end date
    Given a customer with a positive balance
    And a merchant
    And a valid start date
    And a valid token
    When the customer makes a purchase from the merchant
    And the customer generates a report
    Then the customer report is retrieved
    And the customer report contains a transaction
    And the customer transactions contain information about token and amount

  Scenario: A merchant requests a report with an end date
    Given a customer with a positive balance
    And a merchant
    And a valid start date
    And a valid end date
    And a valid token
    When the customer makes a purchase from the merchant
    And the merchant generates a report
    Then the merchant report is retrieved
    And the merchant report contains a transaction
    And the merchant transactions contain information about token and amount

  Scenario: A merchant requests a report without an end date
    Given a customer with a positive balance
    And a merchant
    And a valid start date
    And a valid token
    When the customer makes a purchase from the merchant
    And the merchant generates a report
    Then the merchant report is retrieved
    And the merchant report contains a transaction
    And the merchant transactions contain information about token and amount

