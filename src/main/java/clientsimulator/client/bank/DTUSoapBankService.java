package clientsimulator.client.bank;

import clientsimulator.client.bank.responses.DTUPayEmptyResponse;
import clientsimulator.client.bank.responses.DTUPayResponse;
import clientsimulator.client.bank.responses.*;
import dtu.ws.fastmoney.*;

import java.math.BigDecimal;
import java.util.List;

/**
 * The implementation class of the Bank Service
 * @author 
 *
 */
public class DTUSoapBankService implements BankService {

    private final dtu.ws.fastmoney.BankService bankService;
    /**
     * The constructor of the class
     */
    public DTUSoapBankService() {
        bankService = new BankServiceService().getBankServicePort();
    }
    /**
     * 
     */
    @Override
    public DTUPayEmptyResponse transferMoneyFromTo(String debtor, String creditor, BigDecimal amount, String description) {
        try {
            bankService.transferMoneyFromTo(debtor, creditor, amount, description);
            return createSuccessResponse();
        } catch (BankServiceException_Exception e) {
            return createErrorResponse(e.toString());
        }
    }
    /**
     * 
     */
    @Override
    public DTUPayResponse<String> createAccountWithBalance(User user, BigDecimal balance) {
        try {
            return createSuccessResponse(bankService.createAccountWithBalance(user, balance));
        } catch (BankServiceException_Exception e) {
            return createErrorResponse(e.toString());
        }
    }
    /**
     * 
     */
    @Override
    public DTUPayResponse<List<AccountInfo>> getAccounts() {
        return createSuccessResponse(bankService.getAccounts());
    }
    /**
     * 
     */
    @Override
    public DTUPayEmptyResponse retireAccount(String accountId) {
        try {
             bankService.retireAccount(accountId);
            return createSuccessResponse();
        } catch (BankServiceException_Exception e) {
            return createErrorResponse(e.toString());
        }
    }
    /**
     * 
     */
    @Override
    public DTUPayResponse<Account> getAccountByCprNumber(String cpr) {
        try {
            return createSuccessResponse(bankService.getAccountByCprNumber(cpr));
        } catch (BankServiceException_Exception e) {
            return createErrorResponse(e.toString());
        }
    }
    /**
     * 
     */
    @Override
    public DTUPayResponse<Account> getAccount(String accountId) {
        try {
            return createSuccessResponse(bankService.getAccount(accountId));
        } catch (BankServiceException_Exception e) {
            return createErrorResponse(e.toString());
        }
    }
    /**
     * Helper method for creating a success response
     * @param <T> type of the response
     * @param response the actual response itself
     * @return DTUPayResponse
     */
    private <T> DTUPayResponse<T> createSuccessResponse(T response) {
        return new DTUPayResponseImpl<T>(response);
    }

    /**
     * Helper method for creating an error response
     * @param <T> type of the response
     * @param errorMsg the error message of the response
     * @return DTUPayResponse
     */
    private <T> DTUPayResponse<T> createErrorResponse(String errorMsg)
    {
        return new DTUPayResponseImpl<T>(new DTUPayErrorMessage(errorMsg));
    }
    /**
     * Helper method for creating a success response for the empty response
     * @return DTUPayEmptyResponse
     */
    private DTUPayEmptyResponse createSuccessResponse() {
        return new DTUPayEmptyResponseImpl();
    }
    /**
     * Helper method for creating an error response
     * @param errorMsg the error message of the response
     * @return DTUPayEmptyResponse
     */
    private DTUPayEmptyResponse creteErrorResponse(String errorMsg){
        return new DTUPayEmptyResponseImpl(new DTUPayErrorMessage(errorMsg));
    }
}
