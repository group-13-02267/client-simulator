package clientsimulator.client.bank;

import clientsimulator.client.bank.responses.DTUPayEmptyResponse;
import clientsimulator.client.bank.responses.DTUPayResponse;
import dtu.ws.fastmoney.Account;
import dtu.ws.fastmoney.AccountInfo;
import dtu.ws.fastmoney.User;

import java.math.BigDecimal;
import java.util.List;
/**
 * Interface for the BankService
 * @author 
 *
 */
public interface BankService {
	/**
	 * Method used for transferring money
	 * @param debtor The id of the user who is the payer
	 * @param creditor The id of the user who is the payee
	 * @param amount The amount of money to be transfered
	 * @param description The description of the payment
	 * @return DTUPayEmptyResponse
	 */
    DTUPayEmptyResponse transferMoneyFromTo(
            String debtor,
            String creditor,
            BigDecimal amount,
            String description);

    /**
     * Method used for creating an account with a pre-existing balance
     * @param user The user object that gets created
     * @param balance The amount of money that the user will have
     * @return DTUPayResponse<String>
     */
    DTUPayResponse<String> createAccountWithBalance(
            User user,
            BigDecimal balance);

    /**
     * Method used for getting all of the accounts
     * @return DTUPayResponse<List<AccountInfo>>>
     */
    DTUPayResponse<List<AccountInfo>> getAccounts();
    
    /**
     * Method for retiring an account
     * @param accountId the account that should be retired
     * @return DTUPayEmptyResponse
     */
    DTUPayEmptyResponse retireAccount(
            String accountId);

    /**
     * Method used to retrieve an account by the CPR number
     * @param cpr the cpr number of the user
     * @return DTUPayResponse<Account>
     */
    DTUPayResponse<Account> getAccountByCprNumber(
            String cpr);

    /**
     * Method used to retrieve an account by the ID
     * @param accountId the id of the account
     * @return DTUPayResponse<Account>
     */
    DTUPayResponse<Account> getAccount(
            String accountId);
}
