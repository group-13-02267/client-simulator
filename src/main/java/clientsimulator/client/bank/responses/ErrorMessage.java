package clientsimulator.client.bank.responses;

/**
 * ErrorMessage interface
 */
public interface ErrorMessage {
    String getMessage();
}
