package clientsimulator.client.bank.responses;

/**
 * Implementation class of the ErrorMessage
 */
public class DTUPayErrorMessage implements ErrorMessage {
    private String message;

    /**
     * Constructor for the class
     * @param message the error message
     */
    public DTUPayErrorMessage(String message) {
        this.message = message;
    }

    /**
     * Method that returns the message
     * @return message
     */
    @Override
    public String getMessage() {
        return message;
    }
}
