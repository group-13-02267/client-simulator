package clientsimulator.client.bank.responses;

/**
 * The implementation class of the Empty Response
 */
public class DTUPayEmptyResponseImpl implements DTUPayEmptyResponse {

    private ErrorMessage errorMessage;
    /**
     * Constructor of the class
     */
    public DTUPayEmptyResponseImpl() {
    }

    /**
     * Alternate constructor of the class
     * @param errorMessage the error message
     */
    public DTUPayEmptyResponseImpl(ErrorMessage errorMessage) {
        this.errorMessage = errorMessage;
    }

    /**
     *
     *
     */
    @Override
    public boolean success() {
        return errorMessage == null;
    }

    /**
     *
     *
     */
    @Override
    public ErrorMessage getErrorMessage() {
        return errorMessage;
    }
}
