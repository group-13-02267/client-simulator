package clientsimulator.client.bank.responses;

/**
 * DTUPayResponse interface that extends the DTUPayEmptyResponse
 * @param <T> Type
 */
public interface DTUPayResponse<T> extends DTUPayEmptyResponse {
    /**
     * Method that returns the response
     * @return response
     */
    T getResponse();
}
