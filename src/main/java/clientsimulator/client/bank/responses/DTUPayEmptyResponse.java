package clientsimulator.client.bank.responses;

/**
 * DTUPayEmptyResponse interface
 */
public interface DTUPayEmptyResponse {
    /**
     * Method that returns success
     * @return boolean true if the ErrorMessage is null, false otherwise
     */
    boolean success();

    /**
     * Method to retrieve the error message
     * @return ErrorMessage
     */
    ErrorMessage getErrorMessage();
}
