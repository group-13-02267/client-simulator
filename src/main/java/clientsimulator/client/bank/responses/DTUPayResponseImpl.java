package clientsimulator.client.bank.responses;

/**
 * Implementation class of DTUPayResponse
 * @param <T> Type
 */
public class DTUPayResponseImpl<T> extends DTUPayEmptyResponseImpl implements DTUPayResponse<T> {

    private T response;
    private ErrorMessage errorMessage;

    /**
     * Constructor of the class
     * @param response custom response
     */
    public DTUPayResponseImpl(T response) {
        this.response = response;
    }

    /**
     * Alternate constructor of the class
     * @param errorMessage custom error message
     */
    public DTUPayResponseImpl(ErrorMessage errorMessage) {
        super(errorMessage);
    }

    /**
     * Method to retrieve the response
     * @return
     */
    @Override
    public T getResponse() {
        return response;
    }
}
