package clientsimulator.client.DTUPay.REST;

import clientsimulator.client.DTUPay.REST.requests.RefundResponse;
import clientsimulator.client.DTUPay.REST.responses.DTUEmptyResponse;
import clientsimulator.client.DTUPay.REST.responses.DTUErrorMessage;
import clientsimulator.client.DTUPay.REST.responses.DTUResponse;
import clientsimulator.client.DTUPay.responses.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import javax.ws.rs.core.Response;
import java.lang.reflect.Type;
import java.util.List;

/**
 * Abstract class containing utility methods for REST clients.
 * @author Nick
 */
public abstract class DTURestClient {

    /**
     * Parses a REST response and returns a DTUResponse containing the specified generic type.
     * @param response A response of class javax.ws.rs.core.Response
     * @param valueType Type of the class
     * @param <C> the type of DTUResponse
     * @return parsedResponse
     */
    protected static <C> DTUResponse<C> createRestResponse(javax.ws.rs.core.Response response, Class<C> valueType) {
        int status = response.getStatus();
        if (status < 200 || status >= 300) {
            return new DTUResponse<C>(new DTUErrorMessage("Status code: " + status + ". Error message: "
                    + response.toString()));
        }
        String json = response.readEntity(String.class);
        GsonBuilder gsonBuilder = new GsonBuilder();
        Type responseType = new TypeToken<DTUResponse<C>>() {}.getType();
        DTUResponse<C> parsedResponse = gsonBuilder.create().fromJson(json, responseType);

        return parsedResponse;
    }

    /**
     * Parses a REST response and returns a DTUResponse containing the specified CustomerReport type.
     * @param response A response of class javax.ws.rs.core.Response
     * @return parsedResponse
     */
    protected static DTUResponse<CustomerReport> createCustomerReportResponse(javax.ws.rs.core.Response response) {
        int status = response.getStatus();
        if (status < 200 || status >= 300) {
            return new DTUResponse<>(new DTUErrorMessage("Status code: " + status + ". Error message: "
                    + response.toString()));
        }
        String json = response.readEntity(String.class);
        System.out.println("Customer report json: " + json);
        GsonBuilder gsonBuilder = new GsonBuilder();
        Type responseType = new TypeToken<DTUResponse<CustomerReport>>() {}.getType();
        DTUResponse<CustomerReport> parsedResponse = gsonBuilder.create().fromJson(json, responseType);

        return parsedResponse;
    }

    /**
     * Parses a REST response and returns a DTUResponse containing the specified MerchantReport type.
     * @param response A response of class javax.ws.rs.core.Response
     * @return parsedResponse
     */
    protected static DTUResponse<MerchantReport> createMerchantReportResponse(javax.ws.rs.core.Response response) {
        int status = response.getStatus();
        if (status < 200 || status >= 300) {
            return new DTUResponse<>(new DTUErrorMessage("Status code: " + status + ". Error message: "
                    + response.toString()));
        }
        String json = response.readEntity(String.class);
        System.out.println("Merchant report json: " + json);
        GsonBuilder gsonBuilder = new GsonBuilder();
        Type responseType = new TypeToken<DTUResponse<MerchantReport>>() {}.getType();
        DTUResponse<MerchantReport> parsedResponse = gsonBuilder.create().fromJson(json, responseType);

        return parsedResponse;
    }

    /**
     * Parses a REST response and returns a DTUResponse containing the specified TokenResponse type.
     * @param response A response of class javax.ws.rs.core.Response
     * @return parsedResponse
     */
    protected static DTUResponse<TokenResponse> createTokenResponse(javax.ws.rs.core.Response response) {
        int status = response.getStatus();
        if (status < 200 || status >= 300) {
            return new DTUResponse<TokenResponse>(new DTUErrorMessage("Status code: " + status + ". Error message: "
                    + response.toString()));
        }
        String json = response.readEntity(String.class);
        GsonBuilder gsonBuilder = new GsonBuilder();
        Type responseType = new TypeToken<DTUResponse<TokenResponse>>() {}.getType();
        DTUResponse<TokenResponse> parsedResponse = gsonBuilder.create().fromJson(json, responseType);

        return parsedResponse;
    }




    /**
     * Parses a REST response and returns a DTUResponse containing  a list of the specified generic type.
     * @param response A response of class javax.ws.rs.core.Response
     * @return parsedList
     */
    protected static <C> DTUResponse<List<C>> createRestListResponse(javax.ws.rs.core.Response response, Class<C> valueType) {
        int status = response.getStatus();
        if (status < 200 || status >= 300) {
            return new DTUResponse(new DTUErrorMessage(response.toString()));
        }
        String json = response.readEntity(String.class);
        Type listType = new TypeToken<List<C>>() {}.getType();
        List<C> parsedList = new Gson().fromJson(json, listType);
        return new DTUResponse<List<C>>(parsedList);
    }

    /**
     * Creates an empty rest response
     * @param response Response
     * @return if status in range {200,300}, then return empty response,
     * otherwise return empty response with error message
     */
    public DTUEmptyResponse createEmptyRestResponse(Response response) {
        int status = response.getStatus();
        if (status < 200 || status >= 300) {
            return new DTUEmptyResponse(new DTUErrorMessage(response.toString()));
        }
        return new DTUEmptyResponse();
    }

    /**
     * Method for creating an error response
     * @param statusCode the status code of the error message
     * @param errorMessage the error message
     * @param <C> the type of the DTU response
     * @return DTUResponse with an error message
     */
    protected static <C> DTUResponse<C> createErrorResponse(int statusCode, String errorMessage) {
        return new DTUResponse<C>(new DTUErrorMessage(errorMessage));
    }

    /**
     * Method for creating an empty error response
     * @param errorMessage the error message
     * @return DTUResponse with an error message
     */
    protected static DTUEmptyResponse createEmptyErrorResponse(String errorMessage) {
        return new DTUEmptyResponse(new DTUErrorMessage(errorMessage));
    }

}
