package clientsimulator.client.DTUPay.REST;

import clientsimulator.client.DTUPay.CustomerAPIClient;
import clientsimulator.client.DTUPay.REST.requests.CreateCustomerDTO;
import clientsimulator.client.DTUPay.REST.requests.RequestTokensDTO;
import clientsimulator.client.DTUPay.REST.responses.DTUEmptyResponse;
import clientsimulator.client.DTUPay.REST.responses.DTUResponse;
import clientsimulator.client.DTUPay.responses.*;
import com.google.gson.Gson;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * A REST client for the customer API.
 * @author Nick
 */
public class DTUCustomerRestClient extends DTURestClient implements CustomerAPIClient {

    private final String ENDPOINT = "http://localhost:8080/customer/";

    private Client client;
    private WebTarget target;

    /**
     * Constructor of the class
     */
    public DTUCustomerRestClient() {
        client = ClientBuilder.newClient();
        target = client.target(ENDPOINT);
    }

    /**
     *
     */
    @Override
    public DTUEmptyResponse createCustomer(String firstName, String lastName, String cpr) {
        CreateCustomerDTO request = new CreateCustomerDTO(firstName, lastName, cpr);
        try {
            Invocation.Builder builder = target.request(MediaType.APPLICATION_JSON);
            javax.ws.rs.core.Response response = builder.post(Entity.entity(request, MediaType.APPLICATION_JSON));
            return createEmptyRestResponse(response);
        } catch (WebApplicationException e) {
            return createEmptyRestResponse(e.getResponse());
        }
    }

    /**
     *
     */
    @Override
    public DTUResponse<TokenResponse> requestTokens(String customerResourceId, int amount) {
        RequestTokensDTO request = new RequestTokensDTO(amount);
        try {
            Invocation.Builder builder = target.path(customerResourceId + "/").path("token")
                    .request(MediaType.APPLICATION_JSON);
            javax.ws.rs.core.Response response = builder.post(Entity.entity(request, MediaType.APPLICATION_JSON));
            return createTokenResponse(response);
        } catch (WebApplicationException e) {
            return createTokenResponse(e.getResponse());
        }
    }

    /**
     *
     */
    @Override
    public DTUResponse<CustomerReport> getReport(String customerResourceID, String from) {
        System.out.println("Customer report request: ");
        System.out.println(customerResourceID);
        System.out.println(from);
        try {
            WebTarget reportTarget = target.path(customerResourceID + "/").path("report/").path(from + "/");
            Invocation.Builder builder = reportTarget.request(MediaType.APPLICATION_JSON);
            javax.ws.rs.core.Response response = builder.get();
            return createCustomerReportResponse(response);
        } catch (WebApplicationException e) {
            return createRestResponse(e.getResponse(), CustomerReport.class);
        }
    }

    /**
     *
     */
    @Override
    public DTUResponse<CustomerReport> getReport(String customerResourceID, String from, String to) {
        System.out.println("Customer report request: ");
        System.out.println(customerResourceID);
        System.out.println(from);
        System.out.println(to);
        try {
            WebTarget reportTarget = target.path(customerResourceID + "/").path("report/").path(from + "/").path(to);
            Invocation.Builder builder = reportTarget.request(MediaType.APPLICATION_JSON);
            javax.ws.rs.core.Response response = builder.get();
            return createCustomerReportResponse(response);
        } catch (WebApplicationException e) {
            return createRestResponse(e.getResponse(), CustomerReport.class);
        }
    }

}
