package clientsimulator.client.DTUPay.REST.requests;

/**
 * User Data Transfer Object Base
 */
public class UserDTOBase {
    public String cpr;
    public String firstName;
    public String lastName;
}
