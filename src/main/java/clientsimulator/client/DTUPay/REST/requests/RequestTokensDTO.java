package clientsimulator.client.DTUPay.REST.requests;

/**
 * Request Tokens DTO
 */
public class RequestTokensDTO {
    public int amount;

    /**
     * Constructor of the class
     */
    public RequestTokensDTO() {}

    /**
     * Alternate constructor of the class
     * @param amount the amount of tokens to be requested
     */
    public RequestTokensDTO(int amount) {
        this.amount = amount;
    }
}
