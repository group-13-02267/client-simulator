package clientsimulator.client.DTUPay.REST.requests;

/**
 * Customer Creation Data Transfer Object, extends UserDTOBase
 */
public class CreateCustomerDTO extends UserDTOBase{
    /**
     * Constructor of the class
     */
    public CreateCustomerDTO(){};

    /**
     * Alternate constructor of the class
     * @param firstName the first name of the customer
     * @param lastName the last name of the customer
     * @param cpr the cpr of the customer
     */
    public CreateCustomerDTO(String firstName, String lastName, String cpr){
        this.firstName = firstName;
        this.lastName = lastName;
        this.cpr = cpr;
    }
}
