package clientsimulator.client.DTUPay.REST.requests;

/**
 * Merchant creation Data Transfer Object, extends UserDTOBase
 */
public class CreateMerchantDTO extends UserDTOBase {
    /**
     * Constructor of the class
     */
    public CreateMerchantDTO(){};

    /**
     * Alternate constructor for the class
     * @param firstName the first name of the merchant
     * @param lastName the last name of the merchant
     * @param cpr the cpr of the merchant
     */
    public CreateMerchantDTO(String firstName, String lastName, String cpr){
        this.firstName = firstName;
        this.lastName = lastName;
        this.cpr = cpr;
    }
}
