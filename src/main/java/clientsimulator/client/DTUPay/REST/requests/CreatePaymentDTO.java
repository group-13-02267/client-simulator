package clientsimulator.client.DTUPay.REST.requests;

/**
 * Create Payment Data Transfer Object
 */
public class CreatePaymentDTO {

    public String merchantId;
    public String token;
    public float amount;

    /**
     * Constructor of the class
     */
    public CreatePaymentDTO() {}

    /**
     * Alternate constructor of the class
     * @param merchantId id of the merchant
     * @param token token from the customer
     * @param amount the amount of money to be transferred
     */
    public CreatePaymentDTO(String merchantId, String token, float amount) {
        this.merchantId = merchantId;
        this.token = token;
        this.amount = amount;
    }
}
