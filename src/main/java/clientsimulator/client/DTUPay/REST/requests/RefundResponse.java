package clientsimulator.client.DTUPay.REST.requests;

import java.io.Serializable;

/**
 * Refund Response Data Transfer Object
 */
public class RefundResponse implements Serializable {

    /**
     * Constructor of the class
     * @param success indicates whether the refund succeeded
     */
    public RefundResponse(boolean success) {
        this.success = success;
    }

    /**
     * Alternate constructor of the class
     */
    public RefundResponse() {
    }

    /**
     * Transaction status
     */
    public boolean success;
}
