package clientsimulator.client.DTUPay.REST.requests;

/**
 * Initiate Refund Data Transfer Object
 */
public class InitiateRefundDTO {

    public String merchantId;
    public String token;
    public float amount;

    /**
     * Constructor of the class
     */
    public InitiateRefundDTO() {}

    /**
     * Alternate constructor of the class
     * @param merchantId the id of the merchant
     * @param token the token from the customer
     * @param amount the amount of money to be refunded
     */
    public InitiateRefundDTO(String merchantId, String token, float amount) {
        this.merchantId = merchantId;
        this.token = token;
        this.amount = amount;
    }
}
