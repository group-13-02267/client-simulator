package clientsimulator.client.DTUPay.REST;

import clientsimulator.client.DTUPay.MerchantAPIClient;
import clientsimulator.client.DTUPay.REST.requests.CreateMerchantDTO;
import clientsimulator.client.DTUPay.REST.requests.CreatePaymentDTO;
import clientsimulator.client.DTUPay.REST.requests.InitiateRefundDTO;
import clientsimulator.client.DTUPay.REST.requests.RefundResponse;
import clientsimulator.client.DTUPay.responses.*;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * A REST client for the merchant API.
 * @author Nick, Roberts
 */
public class DTUMerchantRestClient extends DTURestClient implements MerchantAPIClient {

    private final String ENDPOINT = "http://localhost:8080/";

    private Client client;
    private WebTarget merchantTarget;
    private WebTarget paymentTarget;
    private WebTarget refundTarget;

    /**
     * Constructor of the class
     */
    public DTUMerchantRestClient() {
        client = ClientBuilder.newClient();
        merchantTarget = client.target(ENDPOINT + "merchant");
        paymentTarget = client.target(ENDPOINT+"payment");
        refundTarget = client.target(ENDPOINT + "refund");
    }

    /**
     *
     */
    @Override
    public EmptyResponse createMerchant(String firstName, String lastName, String cpr) {
        CreateMerchantDTO request = new CreateMerchantDTO(firstName, lastName, cpr);
        try {
            Invocation.Builder builder = merchantTarget.path("/").request(MediaType.APPLICATION_JSON);
            javax.ws.rs.core.Response response = builder.post(Entity.entity(request, MediaType.APPLICATION_JSON));
            return createEmptyRestResponse(response);
        } catch (WebApplicationException e) {
            return createEmptyRestResponse(e.getResponse());
        }
    }
    /**
     *
     */
    @Override
    public Response<MerchantReport> getReport(String merchantID, String from) {
        System.out.println("Customer report request: ");
        System.out.println(merchantID);
        System.out.println(from);
        try {
            WebTarget reportTarget = merchantTarget.path("/" + merchantID).path("/report/").path(from + "/");
            Invocation.Builder builder = reportTarget.request(MediaType.APPLICATION_JSON);
            javax.ws.rs.core.Response response = builder.get();
            return createMerchantReportResponse(response);
        } catch (WebApplicationException e) {
            return createRestResponse(e.getResponse(), MerchantReport.class);
        }
    }
    /**
     *
     */
    @Override
    public Response<MerchantReport> getReport(String merchantID, String from, String to) {
        System.out.println("Customer report request: ");
        System.out.println(merchantID);
        System.out.println(from);
        System.out.println(to);
        try {
            WebTarget reportTarget = merchantTarget.path("/"+merchantID).path("/report").path("/"+from).path(to);
            Invocation.Builder builder = reportTarget.request(MediaType.APPLICATION_JSON);
            javax.ws.rs.core.Response response = builder.get();
            return createMerchantReportResponse(response);
        } catch (WebApplicationException e) {
            return createRestResponse(e.getResponse(), MerchantReport.class);
        }
    }
    /**
     *
     */
    @Override
    public EmptyResponse initiateRefund(String merchantId, Token token, float amount) {
        InitiateRefundDTO request = new InitiateRefundDTO(merchantId, token.getValue(), amount);
        try {
            Invocation.Builder builder = refundTarget.request(MediaType.APPLICATION_JSON);
            javax.ws.rs.core.Response response = builder.post(Entity.entity(request, MediaType.APPLICATION_JSON));
            return createEmptyRestResponse(response);
        } catch (WebApplicationException e) {
            return createRestResponse(e.getResponse(), RefundResponse.class);
        }
    }
    /**
     *
     */
    @Override
    public EmptyResponse createPayment(String merchantID, Token token, float amount) {
        CreatePaymentDTO request = new CreatePaymentDTO(merchantID, token.getValue(), amount);
        try {
            Invocation.Builder builder = paymentTarget.request(MediaType.APPLICATION_JSON);
            javax.ws.rs.core.Response response = builder.post(Entity.entity(request, MediaType.APPLICATION_JSON));
            return createEmptyRestResponse(response);
        } catch (WebApplicationException e) {
            return createEmptyRestResponse(e.getResponse());
        }
    }

}
