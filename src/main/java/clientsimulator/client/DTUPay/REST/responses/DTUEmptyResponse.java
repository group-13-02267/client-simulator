package clientsimulator.client.DTUPay.REST.responses;

import clientsimulator.client.DTUPay.responses.EmptyResponse;
import clientsimulator.client.DTUPay.responses.ErrorMessage;

/**
 * Implementation of the Empty Response
 */
public class DTUEmptyResponse implements EmptyResponse {

    public ErrorMessage errorMessage;

    /**
     * Constructor of the class
     */
    public DTUEmptyResponse() {}

    /**
     * Alternate constructor of the class
     * @param errorMessage the custom error message
     */
    public DTUEmptyResponse(ErrorMessage errorMessage) {
        this.errorMessage = errorMessage;
    }

    /**
     * Method used for retrieving the error message
     * @return errorMessage
     */
    @Override
    public ErrorMessage getErrorMessage() {
        return errorMessage;
    }

    /**
     * Method indicating the success of the response
     * @return True if errorMessage is null, false otherwise
     */
    @Override
    public boolean success() {
        return errorMessage == null;
    }
}
