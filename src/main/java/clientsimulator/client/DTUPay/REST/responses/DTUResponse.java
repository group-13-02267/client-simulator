package clientsimulator.client.DTUPay.REST.responses;

import clientsimulator.client.DTUPay.responses.ErrorMessage;
import clientsimulator.client.DTUPay.responses.Response;

/**
 * Implementation class of the Response
 * @param <T> Type
 */
public class DTUResponse<T> implements Response<T> {

    public ErrorMessage errorMessage;
    public T response;

    /**
     * Constructor of the class
     * @param response Any instance of a class
     */
    public DTUResponse(T response) {
        this.response = response;
    }

    /**
     * Alternate constructor of the class
     * @param errorMessage a custom ErrorMessage
     */
    public DTUResponse(ErrorMessage errorMessage) {
        this.errorMessage = errorMessage;
    }

    /**
     * Method used to retrieve the value
     * @return value
     */
    @Override
    public T getValue() {
        return response;
    }

    /**
     * Method used to retrieve the ErrorMessage
     * @return errorMessage
     */
    @Override
    public ErrorMessage getErrorMessage() {
        return errorMessage;
    }

    /**
     * Method used to retrieve whether the response was successful
     * @return True if errorMessage is null, false otherwise
     */
    @Override
    public boolean success() {
        return errorMessage == null;
    }

    /**
     * Custom method for converting this Class to String
     * @return error message if error message is not null, response.toString() otherwise
     */
    @Override
    public String toString() {
        if (errorMessage != null) {
            return errorMessage.getMessage();
        } else {
            return response.toString();
        }
    }
}
