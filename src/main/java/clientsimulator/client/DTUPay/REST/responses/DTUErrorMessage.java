package clientsimulator.client.DTUPay.REST.responses;

import clientsimulator.client.DTUPay.responses.ErrorMessage;

/**
 * Implementation class of the Error Message
 * @author Nick
 */
public class DTUErrorMessage implements ErrorMessage {

    public String message;

    /**
     * Constructor of the class
     * @param message custom error message string
     */
    public DTUErrorMessage(String message) {
        this.message = message;
    }

    /**
     * Method used to retrieve the message
     * @return message
     */
    @Override
    public String getMessage() {
        return message;
    }
}
