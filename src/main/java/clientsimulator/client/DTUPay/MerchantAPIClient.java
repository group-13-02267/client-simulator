package clientsimulator.client.DTUPay;

import clientsimulator.client.DTUPay.responses.*;

import java.util.List;

/**
 * Merchant API Client interface
 */
public interface MerchantAPIClient {

    /**
     * Method used to create a merchant
     * @param firstName the first name of the merchant
     * @param lastName the last name of the merchant
     * @param cpr the cpr number of the merchant
     * @return EmptyResponse object
     */
    public EmptyResponse createMerchant(String firstName, String lastName, String cpr);

    /**
     * Method for getting a report
     * @param merchantID the id of the merchant(currently cpr)
     * @param from starting date
     * @return Response object
     */
    public Response<MerchantReport> getReport(String merchantID, String from);
    /**
     * Alternate method for getting a report
     * @param merchantID the id of the merchant(currently cpr)
     * @param from starting date
     * @param to ending date
     * @return Response object
     */
    public Response<MerchantReport> getReport(String merchantID, String from, String to);

    /**
     * Method used to initiate a refund
     * @param merchantId the id of the merchant
     * @param token the token from the customer
     * @param amount the amount to be refunded
     * @return EmptyResponse object
     */
    public EmptyResponse initiateRefund(String merchantId, Token token, float amount);

    /**
     * Method used to create a payment
     * @param merchantID the id of the merchant
     * @param token the token from the customer
     * @param amount the amount to be refunded
     * @return EmptyResponse object
     */
    public EmptyResponse createPayment(String merchantID, Token token, float amount);

}
