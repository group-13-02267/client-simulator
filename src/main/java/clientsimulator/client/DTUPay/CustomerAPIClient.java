package clientsimulator.client.DTUPay;

import clientsimulator.client.DTUPay.responses.*;

/**
 * The Customer API Client interface
 */
public interface CustomerAPIClient {

    /**
     * Method used to create a customer
     * @param firstName the first name of the customer
     * @param lastName the last name of the customer
     * @param cpr the cpr of the customer
     * @return EmptyResponse object
     */
    public EmptyResponse createCustomer(String firstName, String lastName, String cpr);

    /**
     * Method used to request tokens
     * @param customerCpr cpr number of the customer
     * @param amount the amount of requested tokens
     * @return Response object
     */
    public Response<TokenResponse> requestTokens(String customerCpr, int amount);

    /**
     * Method used to get a report
     * @param customerId the id of the customer(currently cpr)
     * @param from starting date
     * @return Response object
     */
    public Response<CustomerReport> getReport(String customerId, String from);

    /**
     * Alternate method for getting a report
     * @param customerResourceID the id of the customer(currently cpr)
     * @param from starting date
     * @param to ending date
     * @return Response object
     */
    public Response<CustomerReport> getReport(String customerResourceID, String from, String to);

}
