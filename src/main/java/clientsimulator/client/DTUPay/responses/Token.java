package clientsimulator.client.DTUPay.responses;

/**
 * Token class
 */
public class Token {
    public String value;

    /**
     * Constructor of the class
     * @param value token value
     */
    public Token(String value) {
        this.value = value;
    }

    /**
     * Alternate constructor of the class
     */
    public Token() {

    }

    /**
     * Method used for setting the value of the token
     * @param value token value
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Method used for getting the value of the token
     * @return token value
     */
    public String getValue() {
        return value;
    }
}
