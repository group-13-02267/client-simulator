package clientsimulator.client.DTUPay.responses;

/**
 * The merchant class
 * @author 
 *
 */
public class Merchant {
    public String cpr;
    public String firstName;
    public String lastName;
/**
 * Constructor of the class
 */
    public Merchant() {}

    /**
     * Alternate constructor of the class
     * @param cpr the cpr of the merchant
     * @param firstName the first name of the merchant
     * @param lastName the last name of the merchant
     */
    public Merchant(String cpr, String firstName, String lastName) {
        this.cpr = cpr;
        this.firstName = firstName;
        this.lastName = lastName;
    }
}
