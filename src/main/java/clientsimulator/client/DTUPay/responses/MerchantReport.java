package clientsimulator.client.DTUPay.responses;


import java.util.List;
/**
 * Class for the merchant report
 * @author 
 *
 */
public class MerchantReport {

    public boolean success;
    public String report;
/**
 * Constructor of the class
 */
    public MerchantReport() {}
    /**
     * Alternate constructor for the class
     * @param success indicates whether the report was retrieved successfully
     * @param report the returned report
     */
    public MerchantReport(boolean success, String report) {
        this.success = success;
        this.report = report;
    }

    /**
     * Method to retrieve the success variable
     */
    public boolean getSuccess() {
        return success;
    }

    /**
     * Method to retrieve the report
     */
    public String getReport() {
        return report;
    }
}
