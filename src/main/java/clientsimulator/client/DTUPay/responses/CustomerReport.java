package clientsimulator.client.DTUPay.responses;

import java.util.List;

/**
 * The class for the customer report
 * @author 
 *
 */
public class CustomerReport {

    public boolean success;
    public String report;
/**
 * Constructor for the class
 */
    public CustomerReport() {}

    /**
     * Alternate constructor for the class
     * @param success indicates whether the report was retrieved successfully
     * @param report the returned report
     */
    public CustomerReport(boolean success, String report) {
        this.success = success;
        this.report = report;
    }

    /**
     * Method to retrieve success variable
     * @return success
     */
    public boolean getSuccess() {
        return success;
    }

    /**
     * Method to retrieve the report
     * @return success
     */
    public String getReport() {
        return report;
    }

}
