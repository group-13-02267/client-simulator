package clientsimulator.client.DTUPay.responses;

/**
 * Customer transaction class that extends the base transaction
 * @author 
 *
 */
public class CustomerTransaction extends BaseTransaction {

    public String customerId;
/**
 * Constructor for the class
 */
    public CustomerTransaction() {}
/**
 * Alternate constructor for the class
 * @param token the token of the transaction
 * @param amount the amount of the transaction
 * @param date the date of the transaction
 * @param customerId the customer id
 */
    public CustomerTransaction(String token, float amount, String date, String customerId) {
        super(token, amount, date);
        this.customerId = customerId;
    }
/**
 * Method to retrieve the customerId
 * @return customerId
 */
    public String getCustomerId() {
        return customerId;
    }
}
