package clientsimulator.client.DTUPay.responses;
/**
 * Merchant transaction class that extends the base transaction
 * @author 
 *
 */
public class MerchantTransaction extends BaseTransaction {

    private String merchantId;
/**
 * The constructor of the class
 */
    public MerchantTransaction() {}
/**
 * The alternate constructor of the class
 * @param token the token of the transaction
 * @param amount the amount of the transaction
 * @param date the date of the transaction
 * @param merchantId the merchant id
 */
    public MerchantTransaction(String token, float amount, String date, String merchantId) {
        super(token, amount, date);
        this.merchantId = merchantId;
    }
/**
 * Method to retrieve the merchant id
 * @return merchantId
 */
    public String getMerchantId() {
        return merchantId;
    }
}
