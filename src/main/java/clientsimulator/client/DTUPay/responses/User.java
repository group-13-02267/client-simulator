package clientsimulator.client.DTUPay.responses;

/**
 * User interface
 */
public interface User {
    /**
     * Method used to retrieve the cpr
     * @return cpr
     */
    String getCpr();

    /**
     * Method to retrieve the first name
     * @return firstName
     */
    String getFirstName();

    /**
     * Method to retrieve the last name
     * @return lastName
     */
    String getLastName();

    /**
     * Boolean that represents whether the user is valid
     * @return
     */
    boolean isValid();
}
