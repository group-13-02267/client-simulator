package clientsimulator.client.DTUPay.responses;
/**
 * The class for the Customer
 * @author 
 *
 */
public class Customer {
    public String cpr;
    public String firstName;
    public String lastName;

    /**
     * Constructor for the class
     */
    public Customer() {}

    /**
     * Alternate constructor for the class
     * @param cpr the cpr number
     * @param firstName the first name
     * @param lastName the last name
     */
    public Customer(String cpr, String firstName, String lastName) {
        this.cpr = cpr;
        this.firstName = firstName;
        this.lastName = lastName;
    }
}
