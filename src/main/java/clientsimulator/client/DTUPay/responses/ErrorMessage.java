package clientsimulator.client.DTUPay.responses;

/**
 * Interface for Error Messages
 * @author 
 *
 */
public interface ErrorMessage {
    /**
     * Method to retrieve the message
     * @return message
     */
    public String getMessage();

}
