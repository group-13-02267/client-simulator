package clientsimulator.client.DTUPay.responses;

import java.time.LocalDate;

/**
 * Pay transaction class that extends the base transaction class
 * @author 
 *
 */
public class PayTransaction extends BaseTransaction {

    public String id;
    public String merchantId;
    public String customerId;
/**
 * Constructor of the class
 */
    public PayTransaction() {}
/**
 * Alternate constructor of the class
 * @param token the token of the transaction
 * @param amount the amount of the transaction
 * @param id the id of the transaction
 * @param merchantId the merchant id
 * @param customerId the customer id
 * @param date the date of the transaction
 */
    public PayTransaction(String token, float amount, String id, String merchantId,
                          String customerId, String date) {
        super(token, amount, date);
        this.id = id;
        this.merchantId = merchantId;
        this.customerId = customerId;
    }
/**
 * Method to retrieve the merchant id
 * @return merchantId
 */
    public String getMerchantId() {
        return merchantId;
    }

   /**
    * Method to retrieve transaction id
    * @return id
    */
    public String getId() {
        return id;
    }
/**
 * Method to retrieve the customer id
 * @return customerId
 */
    public String getCustomerId() {
        return customerId;
    }
}
