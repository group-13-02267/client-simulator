package clientsimulator.client.DTUPay.responses;

import java.util.List;

/**
 * Token response class
 */
public class TokenResponse {
    public String id;
    public List<Token> tokens;
    public boolean status;

    /**
     * Constructor of the class
     */
    public TokenResponse() { }

    /**
     * Alternate constructor of the class
     * @param id id of the token response
     * @param tokens list of tokens
     * @param status status boolean
     */
    public TokenResponse(String id, List<Token> tokens,  boolean status) {
        this.id = id;
        this.tokens = tokens;
        this.status = status;
    }

    /**
     * Method used to retrieve the id of the token response
     * @return id
     */
    public String getId() {
        return id;
    }

    /**
     * Method used to retrieve the list of tokens
     * @return tokens
     */
    public List<Token> getTokens() {
        return tokens;
    }

    /**
     * Method used to retrieve the status
     * @return status
     */
    public boolean getStatus() {
        return status;
    }

    /**
     * Method used for setting the id of the token response
     * @param id id of the token response
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Method used for setting the token list
     * @param tokens list of Token
     */
    public void setTokens(List<Token> tokens) {
        this.tokens = tokens;
    }

    /**
     * Method used for setting the status
     * @param status status of the response
     */
    public void setStatus(boolean status) {
        this.status = status;
    }
}
