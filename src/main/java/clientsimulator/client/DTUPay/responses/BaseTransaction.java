package clientsimulator.client.DTUPay.responses;
/**
 * Base Transaction Class
 * @author 
 *
 */
public abstract class BaseTransaction {

    public String token;
    public float amount;
    public String date;
/**
 * Constructor for the Class
 */
    protected BaseTransaction(){}
/**
 * Alternate constructor for the Class
 * @param token the token of the transaction
 * @param amount the amount to be transfered
 * @param data the date when the transaction happens
 */
    public BaseTransaction(String token, float amount, String date) {
        this.token = token;
        this.amount = amount;
        this.date = date;
    }

    /**
     * Method for getting a token
     * @return token
     */
    public String getToken() {
        return token;
    }

    /**
     * Method for getting the amount
     * @return amount
     */
    public float getAmount() {
        return amount;
    }

    /**
     * Method for getting the date
     * @return date
     */
    public String getDate() {
        return date;
    }
}
