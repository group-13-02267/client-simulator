package clientsimulator.client.DTUPay.responses;

/**
 * The interface used to create empty responses
 * @author
 *
 */
public interface EmptyResponse {

    public ErrorMessage getErrorMessage();
    public boolean success();

}
