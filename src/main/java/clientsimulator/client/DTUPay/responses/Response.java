package clientsimulator.client.DTUPay.responses;

/**
 * Interface for the Response, extends the EmptyResponses
 * @param <T>
 */
public interface Response<T> extends EmptyResponse {
    /**
     * Method to retrieve the value of the response
     * @return Object of Type T
     */
    public T getValue();

}
